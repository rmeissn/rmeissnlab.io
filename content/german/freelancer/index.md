---
# meta description
title: "Webseite von Roy Meissner"
description: "Auf dieser Seite erfahren Sie mehr zu meiner Freiberuflichkeit und meinem Angebot"
draft: false
includeHeader: true
type: home
---

{{< attitude title="Under Construction" subtitle="Bald gibt es hier mehr Inhalt zu finden" >}}
<!-- <div class="row text-center">
	{{< attitudeIcon iconClass="fab fa-skyatlas" color="#0066ff">}}
	{{< attitudeText text="Ich bin der Meinung, eine Kombination von **Ästethik**, **Funktion** und **Pragmatismus** ist genau die Mischung, die großartige Produkte ausmacht. Erlaubt man Menschen, all dies einzubringen, entsteht der Motor unserer Gesellschaft: **Innovation**." >}}
	{{< attitudeIcon iconClass="fab fa-pagelines" color="#20A020" >}}
</div>

<div class="row text-center">
	{{< attitudeText text="Seit meiner Kindheit interessiere ich mich dafür, das Beste aus **verschiedenen Bereichen** zusammenzubringen. **Zusammenarbeit** und das **teilen von Wissen** stellen Gundpfeiler dar, die die Welt verbessern." >}}
	{{< attitudeIcon iconClass="fas fa-heartbeat" color="#ff3300" >}}
	{{< attitudeText text="Unser **Planet** und unsere **Mitmenschen** sind die zwei wertvollsten Güter, die wir besitzen. Vor allem in der heutigen Zeit sollte es jedem erlaubt sein in einer **freundlichen und Umweltbewussten Umgebung** zu leben und zu arbeiten." >}}
</div>

<div class="row text-center">
	{{< attitudeIcon iconClass="fas fa-bolt">}}
	{{< attitudeText text="**Interessen** sind etwas, dass Menschen ausmacht. Egal ob Musik, Technik, Sport oder Umwelt. Nur wer Interessen hat und **Neues ausprobiert** kann sich **weiterentwickeln**, egal ob durch Fehlschlag oder Erfolg." >}}
	{{< attitudeIcon iconClass="fas fa-users" color="#6D6D6D" >}}
</div> -->
{{< /attitude >}}
